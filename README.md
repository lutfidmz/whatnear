# APP WHAT'S NEAR?

#### Find nearby place by your thumb

Apps that finds nearby places by user's location.

### --Links

[Heroku](http://app-whatsnear.herokuapp.com/ "Heroku")
[API Documentation](http://app-whatsnear.herokuapp.com/docs/index.html "Documentation")
[Video Documentation](https://drive.google.com/drive/folders/1oPpMJUMChaH8WZH9S7phfokSqGC2ZSO4?usp=sharing)

Author : [Lutfidmz](https://www.linkedin.com/in/lutfidmz/ "Lutfidmz")
Programmè : `Jabar Coding Camp - NodeJS (AdonisJS)`
Website : [lutfidmz.tech](http://lutfidmz.tech "lutfidmz.tech")

### -- Seeding

There are two seeders, stick to deployment guide

#### --- 1. Places

> Using API from [OpenData JabarProv](https://opendata.jabarprov.go.id/id/dataset/kode-wilayah-dan-nama-wilayah-desa-kelurahan-di-jawa-barat)

#### --- 2. Categories

| category_id | category_name            |
| ----------- | ------------------------ |
| 1           | Sekolah Dasar            |
| 2           | Sekolah Menengah Pertama |
| 3           | Sekolah Menengah Atas    |
| 4           | Tempat Ibadah            |
| 5           | Rumah Sakit              |
| 6           | Puskesmas                |
| 7           | Kantor Pemerintah        |

### --Deploy

`Setup your connection in .env first`

```sh
node ace swagger:generate
npm run build --prod
npm run postbuild
npm ci --production

node ace migration:run
node ace db:seed

node ace generate:key (copy key on terminal to .env in build folder)
node server.js
```
