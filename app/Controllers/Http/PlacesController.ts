import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Database from '@ioc:Adonis/Lucid/Database'

export default class PlacesController {
    public async search({ request, response }: HttpContextContract) {
        const getPlaceSchema = schema.create({
            latitude: schema.number(),
            longitude: schema.number(),
            category_id: schema.number.optional()
        })

        const payload = await request.validate({ schema: getPlaceSchema })

        let query_category = ''
        if (payload.category_id !== undefined) {
            query_category = `AND category_id = ${payload.category_id}`
        }

        try {
            const places = await Database.rawQuery(`SELECT * FROM 
                                                (
                                                SELECT id, name, category_id, city_name, district_name, latitude, longitude
                                                ,(
                                                6371 *
                                                acos(cos(radians(${payload.latitude})) * 
                                                cos(radians(latitude)) * 
                                                cos(radians(longitude) - 
                                                radians(${payload.longitude})) + 
                                                sin(radians(${payload.latitude})) * 
                                                sin(radians(latitude)))
                                                ) AS distances
                                                FROM places
                                                
                                                ) AS al
                                                where distances < 5 
                                                ${query_category} 
                                                ORDER BY distances
                                                `)
            response.ok({ data: places.rows, total: Object.keys(places.rows).length })
        } catch (error) {
            response.badRequest(error)
        }
    }
}
