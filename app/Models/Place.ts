import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Category from './Category'

export default class Place extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public kemendagri_id: number

  @column()
  public category_id: number

  @column()
  public name: string

  @column()
  public city_name: string

  @column()
  public district_name: string

  @column()
  public village_name: string

  @column()
  public latitude: number

  @column()
  public longitude: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Category)
  public category: BelongsTo<typeof Category>
}
