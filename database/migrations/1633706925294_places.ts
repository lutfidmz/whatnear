import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Places extends BaseSchema {
  protected tableName = 'places'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('kemendagri_id').notNullable()
      table.string('name', 100)
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.string('city_name')
      table.string('district_name')
      table.string('village_name')
      table.float('latitude')
      table.float('longitude')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
