import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Category from 'App/Models/Category'

export default class CategorySeeder extends BaseSeeder {
  public async run() {
    await Category.createMany([
      { name: "Sekolah Dasar" },
      { name: "Sekolah Menengah Pertama" },
      { name: "Sekolah Menengah Atas" },
      { name: "Tempat Ibadah" },
      { name: "Rumah Sakit" },
      { name: "Puskesmas" },
      { name: "Kantor Pemerintah" }
    ])
  }
}
