import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Place from 'App/Models/Place'
import axios from 'axios'
const URL = 'https://data.jabarprov.go.id/api-backend/bigdata/diskominfo/od_kode_wilayah_dan_nama_wilayah_desa_kelurahan'
enum enumLevel {
  KOTA = 'KOTA',
  KEC = 'KEC',
  KEL = 'KEL'
}

enum enumKategori {
  SD, // Sekolah Dasar
  SMP, // Sekolah Menengah Pertama
  SMA, // Sekolah Menengah Atas
  TI, // Tempat Ibadah
  RS, // Rumah Sakit
  PU,// Puskesmas
  KP,// Kantor Pemerintah
}

export default class PlaceSeeder extends BaseSeeder {
  public async run() {
    console.log(`=>>> Start seeding Places`);

    let newPlaces = [{}]
    const getRandomPlace = async (level: enumLevel, qty: number, query: string, kategori: enumKategori) => {
      let nama_kategori: string = ""
      let nama_level: string = ""
      let places = [{}]

      switch (kategori) {
        case 0:
          nama_kategori = 'SD'
          break;
        case 1:
          nama_kategori = 'SMP'
          break;
        case 2:
          nama_kategori = 'SMA'
          break;
        case 3:
          nama_kategori = 'Tempat Ibadah'
          break;
        case 4:
          nama_kategori = 'Rumah Sakit'
          break;
        case 5:
          nama_kategori = 'Puskesmas'
          break;
        case 6:
          nama_kategori = 'Kantor Pemerintah'
          break;
        default:
          console.log(kategori);
          break;
      }

      switch (level) {
        case 'KOTA':
          nama_level = 'Kota'
          break;
        case 'KEC':
          nama_level = 'Kecamatan'
          break;
        case 'KEL':
          nama_level = 'Kelurahan'
          break;

        default:
          break;
      }

      console.log(`=>>> API : retrieving ${qty} ${nama_kategori} ${nama_level} `);

      await axios.get(URL + `?search=${query}`)
        .then(response => {
          const data_kemdagri = response.data['data']
          const max = Object.keys(data_kemdagri).length
          let index: number
          let checkName: string
          let nama_tempat: string

          // loop sejumlah qty data yang di butuhkan 
          places.shift()
          for (let i = 0; i < qty; i++) {
            // get random location 
            index = Math.floor(Math.random() * max)
            checkName = data_kemdagri[index]['kemendagri_kota_nama']

            // validate checkName 
            while (checkName.substr(0, 4) == 'KAB.' || checkName == null) {
              index = Math.floor(Math.random() * max)
              checkName = data_kemdagri[index]['kemendagri_kota_nama']
            }

            switch (level) {
              case "KEC":
                checkName = data_kemdagri[index]['kemendagri_kecamatan_nama']
                break;
              case "KEL":
                checkName = data_kemdagri[index]['kemendagri_kelurahan_nama']
                break;
            }

            if (qty > 1) {
              nama_tempat = nama_kategori.toUpperCase() + ' ' + checkName + ' ' + (i + 1)
            } else {
              nama_tempat = nama_kategori.toUpperCase() + ' ' + checkName
            }
            places.push({
              kemendagri_id: data_kemdagri[index]['id'],
              name: nama_tempat,
              category_id: kategori + 1,
              city_name: data_kemdagri[index]['kemendagri_kota_nama'],
              district_name: 'KECAMATAN ' + data_kemdagri[index]['kemendagri_kecamatan_nama'],
              village_name: 'KELURAHAN ' + data_kemdagri[index]['kemendagri_kelurahan_nama'],
              latitude: data_kemdagri[index]['latitude'],
              longitude: data_kemdagri[index]['longitude'],
            })
          }
          // console.log(places);
        })
        .catch(error => console.log(error))
      return places
    }

    let kotaKP = await getRandomPlace(enumLevel.KOTA, 1, 'kota bandung', enumKategori.KP)
    let kotaRS = await getRandomPlace(enumLevel.KOTA, 3, 'kota bandung', enumKategori.RS)
    let kotaSMA = await getRandomPlace(enumLevel.KOTA, 20, 'kota bandung', enumKategori.SMA)
    let kecPU = await getRandomPlace(enumLevel.KEC, 5, 'kota bandung', enumKategori.PU)
    let kecSMP = await getRandomPlace(enumLevel.KEC, 3, 'kota bandung', enumKategori.SMP)
    let kecKP = await getRandomPlace(enumLevel.KEC, 1, 'kota bandung', enumKategori.KP)
    let kelSD = await getRandomPlace(enumLevel.KEL, 5, 'kota bandung', enumKategori.SD)
    let kelTI = await getRandomPlace(enumLevel.KEL, 20, 'kota bandung', enumKategori.TI)
    let kelKP = await getRandomPlace(enumLevel.KEL, 1, 'kota bandung', enumKategori.KP)

    newPlaces = [...kotaKP, ...kotaRS, ...kotaSMA, ...kecPU, ...kecSMP, ...kecKP, ...kelSD, ...kelTI, ...kelKP]
    // console.log('\n\n==========new places=========\n', newPlaces);

    try {
      console.log('===> saving to database');
      await Place.createMany(newPlaces)
    } catch (error) {
      console.log(error);
    }
  }
}
